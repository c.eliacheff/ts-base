// const mysql = require('mysql2');
//
// require('dotenv').config();
//
// const {
//   APP_DB_USER,
//   APP_DB_PASS,
//   APP_DB_HOST,
//   APP_DB_DEV_DB_NAME,
//   APP_DB_TEST_DB_NAME,
//   NODE_ENV
// } = process.env;
//
// const dbName = NODE_ENV === "development"
//   ? APP_DB_DEV_DB_NAME
//   : APP_DB_TEST_DB_NAME;
//
// const connection = mysql.createConnection({
//   host: APP_DB_HOST,
//   user: APP_DB_USER,
//   password: APP_DB_PASS
// });
//
// connection.connect((err) => {
//   if (err) throw err;
//   connection.query(`CREATE DATABASE ${dbName}`, (err, result) => {
//
//     if (err && err.code === "ER_DB_CREATE_EXISTS") {
//       console.log('Db already created');
//       process.exit(0);
//     }
//
//     if (err) {
//       throw err;
//     }
//
//     console.log('Created db');
//     process.exit(0);
//   })
// });
