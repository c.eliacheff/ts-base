// const mysql = require('mysql2');
//
// require('dotenv').config();
//
// const {
//   APP_DB_USER,
//   APP_DB_PASS,
//   APP_DB_HOST,
//   APP_DB_DEV_DB_NAME,
//   APP_DB_TEST_DB_NAME,
//   NODE_ENV
// } = process.env;
//
// const dbName = NODE_ENV === "development"
//   ? APP_DB_DEV_DB_NAME
//   : APP_DB_TEST_DB_NAME;
//
// const connection = mysql.createConnection({
//   host: APP_DB_HOST,
//   user: APP_DB_USER,
//   password: APP_DB_PASS
// });
//
// connection.connect((err) => {
//   if (err) throw err;
//   connection.query(`DROP SCHEMA ${dbName}`, (err, result) => {
//     if (err && err.code === "ER_DB_DROP_EXISTS") {
//       console.log("Already deleted");
//       process.exit(0);
//     }
//
//     if (err) throw err;
//
//     console.log('Deleted db');
//     process.exit(0);
//   })
// });
