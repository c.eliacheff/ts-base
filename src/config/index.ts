const isProduction = process.env.APP_IS_PRODUCTION === 'true';
const isTest = process.env.APP_IS_TEST === 'true';

export { isProduction, isTest };
