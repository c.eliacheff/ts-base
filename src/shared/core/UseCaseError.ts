export abstract class UseCaseError {
  protected constructor(private readonly message: string) {}
}
