import { ResultAsync } from '@shared/core/Result';

export interface UseCase<IRequest, IResponse> {
  handle(request?: IRequest): ResultAsync<IResponse>;
}
