import { UseCaseError } from './UseCaseError';

export class UnexpectedError extends UseCaseError {
  public constructor(err: string) {
    super('An unexpected error occurred.');
    console.error('[AppError]: An unexpected error occurred', err);
  }
}
