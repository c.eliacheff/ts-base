import { Either, EitherAsync } from 'purify-ts';

export type Result<T> = Either<string, T>;
export type ResultAsync<T> = EitherAsync<string, T>;
