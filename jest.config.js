const { defaults: tsjPreset } = require('ts-jest/presets');

module.exports = {
  transform: {
    ...tsjPreset.transform
  },
  testEnvironment: 'node',
  testRegex: './src/.*\\.(test|spec)?\\.(ts|ts)$',
  "roots": [
    "<rootDir>/src"
  ],
  moduleNameMapper: {
    "^@shared/(.*)$": "<rootDir>/src/shared/$1"
  }
};
